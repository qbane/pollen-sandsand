#lang pollen

◊(define h1-color "#999")
◊(define strong-color "green")

body {
  margin: 0;
  padding: 8px;
}

h1 {
  background: ◊|h1-color|;
  color: white;
  padding-left: .4em;
}

h1:before {
  content: '# ';
  color: #ccc;
}

strong {
  color: ◊|strong-color|;
}

#container {
    background: #f1f2f3;
}

#navs {
  max-width: 720px;
  margin: 1em auto;
  padding: 16px;
  text-align: center;
}

.markdown-body {
  box-sizing: border-box;
  min-width: 200px;
  max-width: 992px;
  margin: 0 auto;
  padding: 45px;
}

@media (max-width: 767px) {
  .markdown-body {
    padding: 15px;
  }
}
