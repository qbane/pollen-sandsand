#lang pollen


◊(define-meta site-name "Pollen Hello world")

◊h1{wow}

◊markdown{
# Hello world

Pollen + Markdown
-----------------

+ You **wanted** it — you _got_ it.

+ [search for Racket](https://google.com/search?q=racket)
}

◊h1{I wonder if $2^{\aleph_\alpha} = \aleph_{\alpha+1}$?}


$ax^2+bx+c=0$
